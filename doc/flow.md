```mermaid
graph TD;
subgraph program
 subgraph context
  type1
  buffer1
 end
 subgraph page1
  type1-->element1
  buffer1-->element1
  element1-->action1
 end
end
subgraph object_sources
 source1-->buffer1
end
collection1-->source1
subgraph object_targets
  action1-->target1
end
target1-->collection1
```
