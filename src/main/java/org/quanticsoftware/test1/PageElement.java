package org.quanticsoftware.test1;

import java.util.LinkedHashMap;
import java.util.Map;

public class PageElement {
	public static enum TYPE {
		BUTTON,
		DATAFORM,
		HYPERLINK
	};
	
	public final Map<String, PageElement> elements;
	public final TYPE type;
	public final PageElement parent;
	public TYPE displaytype;
	
	public PageElement(PageElement parent, TYPE type) {
		this.parent = parent;
		this.type = type;
		elements = new LinkedHashMap<>();
	}
	
	public final PageElement button(String name) {
		return element(name, TYPE.BUTTON);
	}
	
	public final PageElement dataform(String name) {
		return element(name, TYPE.DATAFORM);
	}
	
	private final PageElement element(String name, TYPE type) {
		var element = new PageElement(this, type);
		elements.put(name, element);
		return element;
	}
	
	public final PageElement link(String name) {
		return element(name, TYPE.HYPERLINK);
	}
}

