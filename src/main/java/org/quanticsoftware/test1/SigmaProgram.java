package org.quanticsoftware.test1;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.quanticsoftware.automata.builder.AutomataBuilder;
import org.quanticsoftware.automata.core.Concatenate;
import org.quanticsoftware.automata.core.Context;
import org.quanticsoftware.automata.core.DataContext;
import org.quanticsoftware.automata.core.DataObject;
import org.quanticsoftware.automata.core.DataType;
import org.quanticsoftware.automata.core.TypeContext;
import org.quanticsoftware.automata.facilities.Function;
import org.quanticsoftware.automata.lib.Automata;
import org.quanticsoftware.automata.runtime.AutomataProgram;
import org.quanticsoftware.automata.runtime.AutomataRuntime;

public class SigmaProgram {
	private Map<String, Page> pages;
	private Map<String, AutomataProgram> actions;
	private Context context;
	
	public SigmaProgram() {
		pages = new HashMap<>();
	}
	
	public final AutomataProgram action(String name) {
		return actions.get(name);
	}
	
	public static final void build(
			String name,
			SigmaProgram program) throws Exception {
		
		/*
		 * data types
		 */
		var typectx = program.context.typectx();
		
		var page_t = typectx.define("page");
		page_t.add("components");
		
		var pagedef_t = typectx.define("page_definition");
		pagedef_t.add("components");
		
		Automata.seal(program.context);
		
		/*
		 * page builder
		 */
		var pagebuilder = program.context.facility("page_builder");
		
		var pageinstance = pagebuilder.function("instance");
		pageinstance.rule(page_t, s->{
			var opage = s.output();
			opage.set("components", new LinkedHashMap<String, String>());
			
			return opage;
		});
		
		for (var type : PageElement.TYPE.values()) {
			var tname = type.name();
			var bname = Concatenate.execute(tname, "_builder");
			var builder = pagebuilder.function(bname);
			
			builder(typectx, builder, tname);
		}
		
		var logon = program.context.facility("program");
		
		for (var pkey : program.pages.keySet()) {
			var page = program.pages.get(pkey);

			var pagecomponents = new String[page.canvas.elements.size()][2];
			int i = 0;
			
			for (var ekey : page.canvas.elements.keySet()) {
				var element = page.canvas.elements.get(ekey);
				var pagecomponent = new String[] {ekey, element.type.name()};
				pagecomponents[i++] = pagecomponent;
			}
			
			/*
			 * *_page_definition_get
			 */	
			var pdgname = Concatenate.execute(name, "_", pkey, "_page_definition_get");
			var pagedefget = logon.function(pdgname);
			pagedefget.rule(pagedef_t, s->{
				var pagedef = s.output();
				pagedef.set("components", pagecomponents);
				
				return pagedef;
			});
			
			/*
			 * targets
			 */
			var plrtname = Concatenate.execute(name, "_", pkey, "_pageload_response");
			var response = program.context.target(plrtname);
			response.output(page_t);
			
			var test = response.test("test");
			test.output(o->{
				
				@SuppressWarnings("unchecked")
				var components = (Map<String, String>)o.get("components");
				if (components.size() != page.canvas.elements.size())
					return false;
				
				int j = 0;
				for (var  ckey : components.keySet()) {
					var component = pagecomponents[j++];
					if (!ckey.equals(component[0]))
						return false;
					
					var type = components.get(ckey);
					if (!type.equals(component[1]))
						return false;
				}
				
				return true;
			});
		}
		
		program.actions = AutomataBuilder.execute(program.context);
	}
	
	private static final void builder(
			TypeContext typectx,
			Function builder,
			String name) {
		
		var page_t = typectx.get("page");
		var pagedef_t = typectx.get("page_definition");
		
		builder.input(page_t, "page");
		builder.input(pagedef_t, "page_definition");
		builder.rule(page_t, s->{
			var opage = s.get("page");
			
			@SuppressWarnings("unchecked")
			var components = (Map<String, String>)opage.get("components");
			var pos = components.size();
			
			var pdcomponents = (String[][])s.get("page_definition").get("components");
			var pdcomponent = pdcomponents[pos];
			
			if (!pdcomponent[1].equals(name))
				s.fail("invalid component for the position");
			
			components.put(pdcomponent[0], pdcomponent[1]);
			
			return opage;
		});
	}
	
	public static final SigmaProgram instance(Context context) {
		var program = new SigmaProgram();
		program.context = context;
		return program;
	}
	
	public final Page page(String name) {
		return pages.computeIfAbsent(name, k->new Page());
	}
	
	public final DataType typedef(String name) {
		return context.typectx().define(name);
	}
	
	public static final DataObject run(
			SigmaProgram program,
			String action,
			DataContext input) throws Exception {
		return AutomataRuntime.execute(
				program.context,
				program.action(action),
				input);
	}
}

