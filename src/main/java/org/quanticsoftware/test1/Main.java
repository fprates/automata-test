package org.quanticsoftware.test1;

public class Main {
	
	public static final void main(String[] args) throws Exception {
		var sigma = Sigma.instance();
		
		var logon = sigma.program("logon");
		
		var main = logon.page("main");
		main.canvas.dataform("userdata");
		main.canvas.link("recover");
		main.canvas.button("logon");
		
		sigma.build("logon");
		
		sigma.run("logon", "logon_main_pageload_response", null);
	}
}
