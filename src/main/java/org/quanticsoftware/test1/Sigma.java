package org.quanticsoftware.test1;

import java.util.HashMap;
import java.util.Map;

import org.quanticsoftware.automata.core.DataContext;
import org.quanticsoftware.automata.core.DataObject;
import org.quanticsoftware.automata.lib.Automata;

public class Sigma {
	private Map<String, SigmaProgram> programs;
	
	public Sigma() {
		programs = new HashMap<>();
	}
	
	public final void build(String name) throws Exception {
		SigmaProgram.build(name, programs.get(name));
	}

	public static final Sigma instance() {
		return new Sigma();
	}
	
	public final SigmaProgram program(String name) {
		return programs.computeIfAbsent(
				name,
				k->SigmaProgram.instance(Automata.context()));
	}
	
	public final DataObject run(
			String program,
			String action,
			DataContext input) throws Exception {
		return SigmaProgram.run(programs.get(program), action, input);
		
	}
}
